-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th12 04, 2018 lúc 02:54 AM
-- Phiên bản máy phục vụ: 10.1.36-MariaDB
-- Phiên bản PHP: 7.1.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `playgroud-codeigniter-3x`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hosting`
--

CREATE TABLE `hosting` (
  `id` bigint(20) NOT NULL,
  `maHD` bigint(20) NOT NULL,
  `dateCreate` datetime DEFAULT CURRENT_TIMESTAMP,
  `typePackage` bigint(5) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `hosting`
--

INSERT INTO `hosting` (`id`, `maHD`, `dateCreate`, `typePackage`) VALUES
(3, 3, '2015-01-12 00:00:00', 4),
(2, 2, '2015-01-12 00:00:00', 1),
(1, 1, '2015-01-12 00:00:00', 4),
(4, 4, '2018-12-04 06:39:10', 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `hosting`
--
ALTER TABLE `hosting`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
