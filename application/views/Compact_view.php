<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Trang danh sách hợp đồng HOSTING</title>
    <link rel='stylesheet' type='text/css' href='css/style.php' />
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="<?php echo base_url('template/bootstrap/css/bootstrap.min.css'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>


</head>
<body>

<div id="container">
    <div id="body" class="row">
        <!-- Main content -->
        <div class="container">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Danh sách hợp đồng Hosting</h3>

                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <div class="input-group-btn float-right">
                                    <a href="index.php/Compact/create">
                                        <button type="button" class="btn btn-default">Thêm mới</button>
                                    </a>
                                </div>
                                <br/><br/>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="myTable">
                            <tr>
                                <th>ID</th>
                                <th>Mã hợp đồng</th>
                                <th>Ngày tạo</th>
                                <th>Gói hosting</th>
                                <th>Actions</th>
                            </tr>
                            <?php foreach ($hoisting as $data) {

                                ?>
                                <tr>
                                    <td><?= $data['id'] ?></td>
                                    <td><?= $data['maHD'] ?></td>
                                    <td><?= $data['dateCreate'] ?></td>
                                    <td>
                                        <span class="label label-success">
                                            <?php
                                            if ($data['typePackage'] == 1) {
                                                echo 'Cá nhân';
                                            }
                                            elseif ($data['typePackage'] == 2) {
                                                echo 'Doanh nghiệp nhỏ';
                                            }
                                            elseif ($data['typePackage'] == 3) {
                                                echo 'Doanh nghiệp vừa & nhỏ';
                                            }
                                            elseif ($data['typePackage'] == 4) {
                                                echo 'Doanh nghiệp vừa';
                                            }
                                            elseif ($data['typePackage'] == 5) {
                                                echo 'Doanh nghiệp lớn';
                                            }
                                            elseif ($data['typePackage'] == 6) {
                                                echo 'Cá Nhân';
                                            }
                                            elseif ($data['typePackage'] == 7) {
                                                echo 'Cá Nhân';
                                            }
                                            elseif ($data['typePackage'] == 8) {
                                                echo 'Doanh nghiệp vừa & nhỏ';
                                            }
                                            elseif ($data['typePackage'] == 9) {
                                                echo 'Doanh nghiệp vừa & nhỏ';
                                            }
                                            elseif ($data['typePackage'] == 10) {
                                                echo 'Doanh nghiệp vừa';
                                            }
                                            elseif ($data['typePackage'] == 11) {
                                                echo 'Doanh nghiệp vừa +';
                                            }
                                            elseif ($data['typePackage'] == 12) {
                                                echo 'Doanh nghiệp lớn';
                                            }
                                            elseif ($data['typePackage'] == 13) {
                                                echo 'Doanh nghiệp lớn';
                                            }
                                            elseif ($data['typePackage'] == 14){
                                                echo 'Tùy chỉnh';
                                            }
                                            elseif ($data['typePackage'] == 14){
                                                echo 'Tùy biến';
                                            }

                                            else {
                                                echo "Cá Nhân";
                                            }

                                            ?>
                                        </span></td>
                                    <td>

                                        <button type="button" onclick="check(1,<?= $data['id'] ?>)" data-toggle="modal"
                                                data-target="#myModal" class="btn btn-danger">Xóa
                                        </button>
                                        <a href="index.php/Compact/update/<?= $data['id'] ?>">
                                            <button type="button" class="btn btn-default">Cập nhật</button>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                Do you want to delete data?
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-default" onclick="deleterow()" data-dismiss="modal">Yes</button>
            </div>

        </div>
    </div>
</div>

</div>


<!-- creat -- >
<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                Do you want to delete data?
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Không</button>
                <button type="button" class="btn btn-default" onclick="deleterow()" data-dismiss="modal">Yes</button>
            </div>

        </div>
    </div>
</div>

</div>


<!-- end -->


<script>
    var idtem = -1;

    function check(i, id) {
        idtem = id;
    }

    function deleterow() {
        debugger;
        var y = idtem;
        $.ajax({
            type: 'post',
            url: "<?php echo site_url('Compact/delete') ?>/" + y,
            data: {},
            success: function (response) {
                location.reload();
                alert("Delete success");
            }
        });
    }
</script>
</body>
</html>