<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <style>
        tbody{
            white-space: nowrap
        }
    </style>
    <meta charset="utf-8">
    <title>Trang Cập nhật hợp đồng HOSTING</title>
    <link rel="stylesheet" href="<?php echo base_url('template/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>

<body>

<div id="container">

    <div id="body" class="row">
        <div class="container">
            <?= $data_post ?>
        </div>
    </div>
</div>
<button type="button" onclick="update('<?= $id ?>')" class="btn btn-danger" style="float: right;">
    Cập nhật
</button>
</body>
<script>

    function update(id_update) {

        var selValue = $('input[name=hosting_service_package]:checked').val();
        debugger;
        if (selValue != null) {
            $.ajax({
                type: 'post',
                url: "<?php echo site_url('Compact/update_By_id') ?>",
                data: {
                    selValue: selValue,
                    id_update: id_update,
                },
                success: function (response) {
                    if (response > 0) {
                        debugger;
                        alert("Update Success");
                        //location.reload();
                        window.location = "<?=base_url()?>";
                    } else {
                        alert("Update error");
                        location.reload();
                    }
                },
                error: function (data) {
                    debugger;
                    alert("Update error");
                    // location.reload();
                },
            });
        } else {
            //do something
        }
    }
</script>
</html>