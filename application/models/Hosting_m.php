<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set("Asia/Bangkok");

class Hosting_m extends MY_Model
{

    public $primary_key = 'id';
    public $_table = 'hosting';

    public function get_all()
    {
        $query = $this->db->get("hosting");
        $query = $this->db->get("hosting");

        return $query->result_array();
    }

    public function delete_By_id($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('hosting');
    }

    public function insert_By_id($id, $typePackage)
    {
        $data = array(
            'id' => $id + 1,
            'maHD' => $id + 1,
            'dateCreate' => date("Y-m-d H:i:s"),
            'typePackage' => $typePackage
        );
        $this->db->insert('hosting', $data);
        return $this->db->insert_id();
    }

    public function idmax()
    {
        $this->db->from('hosting');
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updatedb_By_typePackage($id, $typePackage)
    {
        $data = array(
            'typePackage' => (int)$typePackage
        );
        $this->db->update('hosting', $data, array('id' => (int)$id));
        return $this->db->affected_rows();
    }
}
/* End of file Hosting_m.php */
/* Location: ./application/models/Hosting_m.php */