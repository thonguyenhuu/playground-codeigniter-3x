<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Bài test 1 : Công ty đang cần một trang nội bộ để bộ phận Sale-Admin có thể tạo và quản lý các hợp đồng dịch vụ hosting cho khách hàng
 * y/c :  
 * 
 * 	- Cần thêm các trường tương ứng vào table hosting trong database để phù hợp với các trường của hợp đồng trong trang test_1/index
 * 	- Trang danh sách func index() - liệt kê tất cả các hợp đồng hosting đang được quản lý
 * 		+ Liệt kê tất cả hợp đồng ("done")
 * 		+ cho phép delete hợp đồng - khi click có hiển thị modal y/c sale-admin xác nhận xóa hợp đồng ("done")
 * 	- Trang tạo mới func create() - khởi tạo một hợp đồng dịch vụ hosting
 * 	- Trang cập nhật func update() - Cập nhật hợp đồng
 * 	- Trang tạo mới và cập nhật dữ liệu yêu cầu : 
 * 	
 * 		+ Hiển thị tương tự các gói dịch vụ hosting tương tự như hình hosting_packages.png ở folder src/image/hosting_packages.png
 * 		+ field gói dịch vụ là một giá trị trong ( là key của tập config hosting ở file config/hosting.php )
 * 			* h5
 *			* h9
 *			* h13
 *			* h16
 *			* h20
 *			* h25
 *			* h45
 *		+ Y/c sử dụng array từ file hosting/config và sử dụng vòng lặp để  hiển thị ra dạng table tương tự ảnh src/image/hosting_packages.png
 * 	
 * 	- Sử dụng các function tích hợp sẵn ở file model Hosting_m
 * 	- Cần validate dữ liệu trước khi insert|update
 */

date_default_timezone_set("Asia/Bangkok");

class Compact extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Hosting_m', 'Dadoiten');
        $this->load->config('hosting');
    }

    public function index()
    {

        $data['template'] = $this->config->item('package_config');

        $datamodel = $this->Dadoiten->get_all();
        $data["hoisting"] = $datamodel; // truyền data vào view
        $this->load->view('Compact_view', $data);
    }

    public function create()
    {
        $data_post = array();
        /*
         * sử dụng $this->input->post() để lấy tất cả dữ liệu được post lên
         * sử dụng $this->hosting_m->insert($data) để tiến hành insert vào database
         */
        $data_post["data_post"] = $this->tablehtml();
        $this->load->view('test_1/create', $data_post);
    }

    public function insert()
    {

        /*
         * sử dụng $this->input->post() để lấy tất cả dữ liệu được post lên
         * sử dụng $this->hosting_m->insert($data) để tiến hành insert vào database
         */
        $id = $this->Dadoiten->idmax()["id"];
        $data_post = $this->input->post("selValue");
        $data = array(
            'id' => (int)$id + 1,
            'maHD' => $id + 1,
            'typePackage' => (int)$data_post,
            'dateCreate' => date("Y-m-d H:i:s"),
        );

        $datamodel = $this->Dadoiten->insert($data, true);
        // chạy dòng dưới cũng được
        //$datamodel = $this->Dadoiten->insert_By_id($id, $data_post);

    }

    public function update($id = 0)
    {
        $data = array('id' => $id);
        /*
         * sử dụng model hosting_m để lấy dữ liệu dựa trên ID , vd : $this->hosting_m->get($id);
         * sử dụng $this->input->post() để lấy tất cả dữ liệu được post lên
         * sử dụng $this->hosting_m->insert($data) để tiến hành insert vào database
         */
        $post = $this->input->post();
        $data_post["data_post"] = $this->tablehtml();
        $data_post["id"] = $id;
        $this->load->view('test_1/update', $data_post);
    }

    public function update_By_id()
    {
        $id = $this->input->post("id_update");
        $data_post = $this->input->post("selValue");
        $data = array(
            'id' => (int)$id,
            'typePackage' => (int)$data_post
        );
        $datamodel = $this->Dadoiten->update($id, $data, true);
        //chạy dòng dưới cũng được
        //$datamodel = $this->Dadoiten->updatedb_By_typePackage($id, $data_post);

        print_r($datamodel);
        exit();
    }

    public function delete($id)
    {
        $data = $this->Dadoiten->delete($id);
        //Xoa du lieu trong db
        print_r($data);
    }

    public function array()
    {
        $datarow = $this->config->item('package_config');
        //Xoa du lieu trong db
        $datacell = array_values($this->config->item('packages')["service"]);
    }

    public function tablehtml()
    {
        $html = '<table border="1" cellpadding="2" cellspacing="1" class="table table-responsive table-bordered table-hover col-6" style="text-align:center;>
                <caption>Chi tiết dịch vụ</caption>
               
                <tbody style="white-space: nowrap" >';
        // lấy từng giá trị trong mảng ra - $config['package_config']
        $datarow = $this->config->item('package_config');

        // lấy từng giá trị trong mảng ra - $config['packages']
        $datacell = array_values($this->config->item('packages')["service"]);
//         echo "<pre>";
//        print_r(array_values( $datarow));exit();

        // 1 - package_config
        foreach ($datarow as $key => $value) {
            //giá trị tháng, năm, không có thì ráng rỗng
            if (!isset($value["unit"])) {
                $value["unit"] = "";
            }
            // In ra ên tiêu đề - package_config
            $html .= '  <tr> <td>' . $value["text"] . '</td>';
            //service - packages
            foreach ($datacell as $keycell => $valuecell) {
                $valuetd = null;
                //lấy từng giá trị và tính toán đổ dữ liệu ra từng ô - mỗi mảng có nhiều kiểu number-string-bool-header đổ ra mảng tương ứng
                if ($value["type"] == "number") {
                    if ($key == "price_year") {
                        $datacell[$keycell][$key] = $datacell[$keycell]["price"] * 12;
                    } elseif ($key == "disk") {
                        $datacell[$keycell][$key] = ($datacell[$keycell]["disk"] > 0) ? $datacell[$keycell]["disk"] / 1000 : 0;
                    }
                    if (isset($datacell[$keycell][$key])) {
                        $valuesubcell = (int)$datacell[$keycell][$key];
                        $valuetd = number_format($valuesubcell) . " " . $value["unit"] . "";
                    }
                } elseif ($value["type"] == "string") {
                    if (isset($datacell[$keycell][$key])) {
                        $valuesubcell = $datacell[$keycell][$key];
                        $valuetd = $valuesubcell . " " . $value["unit"] . "";
                    }
                } elseif ($value["type"] == "bool") {
                    if (isset($datacell[$keycell][$key])) {
                        $valuesubcell = ($datacell[$keycell][$key] > 0) ? "<i style='color:deepskyblue' class=\"fas fa-check\"></i>" : "--";
                        $valuetd = $valuesubcell . " " . $value["unit"] . "";
                    }
                } elseif ($value["type"] == "header") {
                    if (isset($datacell[$keycell][$key])) {
                        $valuesubcell = $datacell[$keycell][$key];
                        $valuetd = '<label>
                                    <input type="radio" name="hosting_service_package" value="' . $keycell . '"><br>
                                       ' . $valuesubcell . '
                                    </label>';
                    }
                }
                $html .= ' <td>' . $valuetd . '</td>';
            }
        }
        $html .= ' </tr></tbody></table>';
        return $html;
    }
}