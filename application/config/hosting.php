<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['packages'] = array(
    'service' => array(
//1
        'h5gb' => array(
            'label' => 'Cá nhân',
            'name' => 'h5gb',
            'price' => 290000 ,
            'month' => NULL,
            'disk' => 5000,
            'bandwidth'  => 170000,
            'sub_domain' => 10,
            'addon_domain' => 1,
            'mysql' => 3,
            'email_webmail' => FALSE,
            'free_register_domain' => FALSE,
            'backup' => 'Hàng tuần',
            'adsoptimize' => TRUE,
            'free_ssl' => TRUE,
            'buy2get1' => TRUE,
            'status' => 'active'
        ),
//2
        'h9gb' => array(
            'label' => 'Doanh nghiệp nhỏ',
            'name' => 'h9gb',
            'price'   => 410000 ,
            'month'    => NULL,
            'disk' => 9000,
            'bandwidth'  => 320000,
            'sub_domain' => 40,
            'addon_domain' => 2,
            'mysql' => 5,
            'email_webmail' => FALSE,
            'free_register_domain' => FALSE,
            'backup' => 'Hàng tuần',
            'adsoptimize' => TRUE,
            'free_ssl' => TRUE,
            'buy2get1' => TRUE,
            'status' => 'active'
        ),
//3
        'h13gb' => array(
            'label' => 'Doanh nghiệp vừa & nhỏ',
            'name' => 'h13gb',
            'price'   => 510000 ,
            'month'    => NULL,
            'disk' => 13000,
            'bandwidth'  => 'Không giới hạn',
            'sub_domain' => 60,
            'addon_domain' => 2,
            'mysql' => 6,
            'email_webmail' => FALSE,
            'free_register_domain' => TRUE,
            'backup' => 'Hàng ngày',
            'adsoptimize' => TRUE,
            'free_ssl' => TRUE,
            'buy2get1' => TRUE,
            'status' => 'active'
        ),
//4
        'h16gb' => array(
            'label' => 'Doanh nghiệp vừa',
            'name' => 'h16gb',
            'price'   => 610000,
            'month'    => NULL,
            'disk' => 16000,
            'bandwidth'  => 'Không giới hạn',
            'sub_domain' => 80,
            'addon_domain' => 3,
            'mysql' => 7,
            'email_webmail' => FALSE,
            'free_register_domain' => TRUE,
            'backup' => 'Hàng ngày',
            'adsoptimize' => TRUE,
            'free_ssl' => TRUE,
            'buy2get1' => TRUE,
            'status' => 'active'
        ),
//5
        'h20gb' => array(
            'label' => 'Doanh nghiệp lớn',
            'name' => 'h20gb',
            'price'   => 710000 ,
            'month'    => NULL,
            'disk' => 20000,
            'bandwidth'  => 'Không giới hạn',
            'sub_domain' => 100,
            'addon_domain' => 3,
            'mysql' => 7,
            'email_webmail' => FALSE,
            'free_register_domain' => TRUE,
            'backup' => 'Hàng ngày',
            'adsoptimize' => TRUE,
            'free_ssl' => TRUE,
            'buy2get1' => TRUE,
            'status' => 'active'
        ),
//6
        'hosting_2gb' => array(
            'label' => 'GÓI CÁ NHÂN',
            'name' => 'hosting_2gb',
            'disk' => 2000,
            'bandwidth'  => 'Không giới hạn',
            'price'   => floor(1980000/12) ,
            'email_webmail' => 60,
            'month'    => NULL,
            'free_register_domain' => FALSE,
            'wpoptimize' => FALSE,
            'backup' => 'Hàng tuần',
            'adsoptimize' => FALSE,
            'free_ssl' => FALSE,
            'buy2get1' => FALSE,
            'status' => 'unactive'
        ),
//7
        'hosting_3gb' => array(
            'label' => 'GÓI CÁ NHÂN',
            'name' => 'hosting_3gb',
            'disk' => 3000,
            'bandwidth'  => 70,
            'price'   => 259000,
            'email_webmail' => 160,
            'month'    => 12,
            'free_register_domain' => FALSE,
            'wpoptimize' => FALSE,
            'backup' => 'Hàng tuần',
            'adsoptimize' => FALSE,
            'free_ssl' => FALSE,
            'buy2get1' => FALSE,
            'status' => 'unactive'
        ),
 //8
        'hosting_4gb' => array(
            'label' => 'GÓI DOANH NGHIỆP VỪA & NHỎ',
            'name' => 'hosting_4gb',   
            'disk' => 4000,       
            'bandwidth'  => 'Không giới hạn',
            'price'   => floor(2940000/12) ,
            'email_webmail' => 240,
            'month'    => NULL,
            'free_register_domain' => FALSE,
            'wpoptimize' => FALSE,
            'backup' => 'Hàng ngày',
            'adsoptimize' => FALSE,
            'free_ssl' => FALSE,
            'buy2get1' => FALSE,
            'status' => 'unactive'
        ),
//9
        'hosting_5gb' => array(
            'label' => 'GÓI DOANH NGHIỆP VỪA & NHỎ',
            'name' => 'hosting_5gb',   
            'disk' => 5000,       
            'bandwidth'  => 160,
            'price'   => 399000,
            'email_webmail' => 'Không giới hạn',
            'month'    => 12,
            'free_register_domain' => FALSE,
            'wpoptimize' => FALSE,
            'backup' => 'Hàng ngày',
            'adsoptimize' => FALSE,
            'free_ssl' => FALSE,
            'buy2get1' => FALSE,
            'status' => 'unactive'
        ),
 //10
        'hosting_6gb' => array(
            'label' => 'GÓI DOANH NGHIỆP VỪA',
            'disk' => 6000, 
            'name' => 'hosting_6gb',
            'price' => floor(3920000/12),
            'bandwidth'  => 'Không giới hạn',
            'email_webmail' => 'Không giới hạn',
            'month'    => NULL,
            'free_register_domain' => FALSE,
            'wpoptimize' => FALSE,
            'backup' => 'Hàng ngày',
            'adsoptimize' => FALSE,
            'free_ssl' => FALSE,
            'buy2get1' => FALSE,
            'status' => 'unactive'
        ),
//11
        'hosting_7gb' => array(
            'label' => 'GÓI DOANH NGHIỆP VỪA +',
            'disk' => 7000, 
            'name' => 'hosting_7gb',
            'price' => 499000,
            'bandwidth'  => 280,
            'email_webmail' => 'Không giới hạn',
            'month'    => 12,
            'free_register_domain' => FALSE,
            'wpoptimize' => FALSE,
            'backup' => 'Hàng ngày ',
            'adsoptimize' => FALSE,
            'free_ssl' => FALSE,
            'buy2get1' => FALSE,
            'status' => 'unactive'
        ),
//12
        'hosting_8gb' => array(
            'label' => 'DOANH NGHIỆP LỚN',
            'name' => 'hosting_8gb',
            'disk' => 8000, 
            'price' => floor(4900000/12),           
            'bandwidth'  => 'Không giới hạn',
            'email_webmail' => 'Không giới hạn',
            'month'    => NULL,
            'free_register_domain' => FALSE,
            'wpoptimize' => FALSE,
            'backup' => 'Hàng ngày',
            'adsoptimize' => FALSE,
            'free_ssl' => FALSE,
            'buy2get1' => FALSE,
            'status' => 'unactive'
        ),
//13
        'hosting_10gb' => array(
            'label' => 'DOANH NGHIỆP LỚN',
            'name' => 'hosting_10gb',
            'disk' => 10000, 
            'price' => 599000,           
            'bandwidth'  => 500,
            'email_webmail' => 'Không giới hạn',
            'month'    => 12,
            'free_register_domain' => FALSE,
            'wpoptimize' => FALSE,
            'backup' => 'Hàng ngày ',
            'adsoptimize' => FALSE,
            'free_ssl' => FALSE,
            'buy2get1' => FALSE,
            'status' => 'unactive'
        ),
//14
        'hosting_orther'    => array(
            'label'         => 'Gói tùy chỉnh',
            'name'          => 'hosting_orther',
            'disk'          => '...', 
            'price'         => 0,
            'bandwidth'     => '...',
            'email_webmail' => '...',
            'month'         => NULL,
            'free_register_domain' => FALSE,
            'wpoptimize' => FALSE,
            'backup' => 'Hàng ngày',
            'adsoptimize' => FALSE,
            'free_ssl' => FALSE,
            'buy2get1' => FALSE,
            'status' => 'unactive'
        ), 
//15
        'hosting_custom'    => array(
            'label'         => 'Gói tùy biến',
            'name'          => 'hosting_custom',
            'disk'          => NULL, 
            'price'         => NULL,

            'bandwidth'  => NULL,
            'sub_domain' => NULL,
            'addon_domain' => NULL,
            'mysql' => NULL,

            'email_webmail' => NULL,
            'month'         => NULL,
            'free_register_domain' => NULL,
            'wpoptimize' => NULL,
            'backup' => NULL,
            'adsoptimize' => NULL,
            'free_ssl' => NULL,
            'buy2get1' => NULL,
            'status' => 'active'
        ), 
    ),
    'default' =>'hosting_2gb'
);

$config['package_config'] = array(
    'label' => ['type' => 'header', 'min' => 0, 'max' => PHP_INT_MAX , 'text' => '', 'unit' => ''],
    'price' => ['type' => 'number', 'min' => 0, 'max' => PHP_INT_MAX , 'text' => 'Giá trị /tháng', 'unit' => ' /tháng'],
    'price_year' => ['type' => 'number', 'min' => 0, 'max' => 100, 'text' => 'Giá trị /năm', 'unit' => ' /năm'],
    'month' => [
        'type' => 'enum',
        'list' => array(
            12  => 'Mua 1',
            36  => 'Mua 2 tặng 1',
            72  => 'Mua 4 tặng 2',
            108 => 'Mua 6 tặng 3',
            144 => 'Mua 8 tặng 4',
        ),
        'text' => 'Số năm đăng ký'
    ],
    'disk' => ['type' => 'number', 'min' => 0, 'max' => 100, 'text' => 'Dung lượng lưu trữ (1GB = 1000MB)', 'unit' => ' GB', 'divide_by' => 1000],
    'bandwidth' => ['type' => 'string', 'min' => 0, 'text' => 'Lưu lượng truy cập', 'unit' => ' Lượt/tháng'],
    
    'sub_domain'    => ['type' => 'number', 'min' => 0, 'max' => 200, 'text' => 'Subdomain'],
    'addon_domain'  => ['type' => 'number', 'min' => 0, 'max' => 200, 'text' => 'Addon domain'],
    'mysql'         => ['type' => 'number', 'min' => 0, 'max' => 10, 'text' => 'MySQL'],

    'backup' => ['type' => 'string', 'list' => [FALSE => 'Không', TRUE => 'Có'], 'text' => 'Sao lưu dữ liệu'],
    'adsoptimize' => ['type' => 'bool', 'list' => [FALSE => 'Không', TRUE => 'Có'], 'text' => 'Tối ưu chạy quảng cáo GG-FB'],
    'free_ssl' => ['type' => 'bool', 'list' => [FALSE => 'Không', TRUE => 'Có'], 'text' => 'Miễn phí SSL'],
    'free_register_domain' => ['type' => 'bool', 'list' => [FALSE => 'Không', TRUE => 'Có'], 'text' => 'Miễn phí tên miền năm đầu'],  
    'email_webmail' => ['type' => 'number', 'min' => 0, 'max' => PHP_INT_MAX , 'text' => 'E-mail/WebMail'],
);

$config['post_type'] = array(
    'task'              =>'hosting-task',
    'content'           =>'hosting-content',
    'content_product'   =>'hosting-content-product',
    'mail'              =>'hosting-mail'
);